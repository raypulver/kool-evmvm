'use strict';

const { expect } = require('chai');
const rpcCall = require('kool-makerpccall');
const fs = require('fs');
const path = require('path');
const easySolc = require('../lib/easy-solc');
const emasm = require('emasm');
const abi = require('web3-eth-abi');

let bytecode;
try {
	console.log('compiling ...');
//	bytecode = easySolc.compile('IndestructibleChecker', fs.readFileSync(path.join(__dirname, '..', 'contracts', 'IndestructibleChecker.sol'), 'utf8')).bytecode;
	console.log('done!');
} catch (e) {
	console.log(e.stack);
//	e.errors.forEach((v) => console.error(v.formattedMessage));
	process.exit(0);
}

const ln = (v) => ((console.log(v)), v);
const call = (method, params = []) => rpcCall(...(['http://localhost:8545', method, params]));
bytecode = require('../contracts/IndestructibleChecker');

describe('indestructible checker', () => {
	it('should test if a contract is indestructible', async () => {
		const [ from ] = await call('eth_accounts');
		const { contractAddress } = await call('eth_getTransactionReceipt', [ await call('eth_sendTransaction', [{
			from,
			gasPrice: 1,
			gas: 6e6,
			data: bytecode
		}]) ]);
    const { contractAddress: simpleSelfdestructor } = await call('eth_getTransactionReceipt', [ await call('eth_sendTransaction', [{
			from,
			gasPrice: 1,
			gas: 6e6,
			data: emasm([
				[
					'bytes:contract:size',
					'dup1',
					'bytes:contract:ptr',
					'0x0',
					'codecopy',
					'0x0',
					'return'
				],
				['bytes:contract', [ emasm([
					'0x60',
					'0x40',
					'mstore',
					'0x0',
					'calldataload',
					'0xe0',
					'shr',
					'dup1',
					'0x44444444',
					'eq',
					'someFn',
					'jumpi',
					'0x66666666',
					'eq',
					'kill',
					'jumpi',
					'stop',
					['someFn', [
						'pop',
						'0x40',
						'mload',
						'dup1',
						'0x4',
						'calldataload',
						'0x1',
						'add',
						'swap1',
						'mstore',
            '0x20',
						'swap1',
						'return'
					]],
					['kill', [
						'origin',
						'selfdestruct'
					]]
		    ]) ]]
			])
		}]) ]);
		expect(Number(await call('eth_call', [{
			to: contractAddress,
			data: '0x' + Array(25).join('0') + simpleSelfdestructor.substr(2)
		}, 'latest']))).to.eql(1);
    const { contractAddress: unreachableSelfDestruct } = await call('eth_getTransactionReceipt', [ await call('eth_sendTransaction', [{
			from,
			gasPrice: 1,
			gas: 6e6,
			data: emasm([
				[
					'bytes:contract:size',
					'dup1',
					'bytes:contract:ptr',
					'0x0',
					'codecopy',
					'0x0',
					'return'
				],
				['bytes:contract', [ emasm([
					'0x60',
					'0x40',
					'mstore',
					'0x0',
					'calldataload',
					'0xe0',
					'shr',
					'dup1',
					'0x44444444',
					'eq',
					'someFn',
					'jumpi',
					'0x66666666',
					'eq',
					'kill',
					'jumpi',
					'stop',
					['someFn', [
						'pop',
						'0x40',
						'mload',
						'dup1',
						'0x4',
						'calldataload',
						'0x1',
						'add',
						'swap1',
						'mstore',
            '0x20',
						'swap1',
						'return'
					]],
					['kill', [
						'origin',
						'pop',
						'safety',
						'jump',
						'selfdestruct'
					]],
					['safety', [
						'0x0',
						'0x0',
						'revert'
					]]
		    ]) ]]
			])
		}]) ]);
		expect(ln(Number(await call('eth_call', [{
			to: contractAddress,
			data: '0x' + Array(25).join('0') + unreachableSelfDestruct.substr(2)
		}])))).to.eql(0);
    const { contractAddress: soliditySelfDestruct } = await call('eth_getTransactionReceipt', [ await call('eth_sendTransaction', [{
			from,
			gasPrice: 1,
			gas: 6e6,
			data: easySolc.compile('Destructible', `contract Destructible {
			    function test() public {
						        selfdestruct(tx.origin);
						    }
		  }`).bytecode
		}]) ]);
		expect(Number(ln(await call('eth_call', [{
			to: contractAddress,
			data: '0x' + Array(25).join('0') + soliditySelfDestruct.substr(2)
		}])))).to.eql(1);
		/*
		const { bytecode: ouchBytecode } = easySolc.compile('Ouch', `pragma solidity ^0.5.0;
		contract Ouch {
			event SomeLog(bytes arg);
      bytes private constant makesItSeemDestructible = hex"0000000000000000000000000000005bff0000000000000000000000000000000000";
      function () external {
        bytes memory damn = makesItSeemDestructible;
				emit SomeLog(damn);
			}
    }`);
    const { contractAddress: brokenSelfDestruct } = await call('eth_getTransactionReceipt', [ await call('eth_sendTransaction', [{
			from,
			gasPrice: 1,
			gas: 6e6,
			data: ouchBytecode
		}]) ]);
		expect(Number(await call('eth_call', [{
			to: contractAddress,
			data: abi.encodeFunctionCall({
				name: 'isPotentiallyDestructible',
				inputs: [{
					name: 'target',
					type: 'address'
				}]
			}, [ brokenSelfDestruct ])
		}]))).to.eql(0);
		*/
	});
	/*
	it('tests what happens when you load data outside memory', async () => {
		expect(await call('eth_call', [{
			data: emasm([
				'0x01',
				'0x0',
				'mstore8',
				'0x0',
				'mload',
				'dup1',
				'0x0',
				'not',
				'0x0',
				'mstore', // writes 0xffff..{32 bytes} to 0x0
				'0x0',
				'mstore', // writes whatever I loaded when memory was only 1 byte large and contained 0x01
        '0x20',
				'0x0',
				'return' // should return 0x01000000..{32 bytes}
			])
		}])).to.eql('0x0100000000000000000000000000000000000000000000000000000000000000');
	});
	*/
});
