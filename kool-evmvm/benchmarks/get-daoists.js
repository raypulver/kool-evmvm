'use strict';

const solc = require('solc');
const loadRemoteVersion = (version) => new Promise((resolve, reject) => solc.loadRemoteVersion(version, (err, result) => err ? reject(err) : resolve(result)));

const fs = require('fs');
const path = require('path');
const rpcCall = require('kool-makerpccall');
const ln = (v) => (v);
const call = (method, params = []) => rpcCall(...ln(['http://localhost:8545', method, params]));
const abi = require('web3-eth-abi');
const easySolc = require('../test/easy-solc');
const encodeParameters = abi.encodeParameters.bind(abi);
const encodeFunctionCall = abi.encodeFunctionCall.bind(abi);

const source = fs.readFileSync(path.join(__dirname, 'EXEDAO-bytecode.txt'), 'utf8');

const addHexPrefix = (s) => s.substr(0, 2) === '0x' ? s : '0x' + s;

(async () => {
	const solc = await loadRemoteVersion('v0.5.9+commit.e560f70d');
	const { bytecode } = source;
  const [ from ] = await call('eth_accounts');
	const ln = (v) => ((console.log(v)), v);
  const {
		contractAddress
	} = await call('eth_getTransactionReceipt', [ await call('eth_sendTransaction', [{
		from,
		data: bytecode,
		gasPrice: 1,
		gas: 6e6
	}]) ])
	const {
		contractAddress: vmAddress
	} = await call('eth_getTransactionReceipt', [ await call('eth_sendTransaction', [{
		from,
		gas: 6e6,
		gasPrice: 1,
		data: require('../src/compile-huff')()
	}]) ]);
	const getDAOistsData = encodeFunctionCall({
		name: 'getDaoists',
		inputs: []
	}, []);
	const { gasUsed: getGasUsed } = await call('eth_getTransactionReceipt', [ await call('eth_sendTransaction', [{
		to: vmAddress,
		from,
		gasPrice: 1,
		gas: 6e6,
		data: encodeParameters([
			'address',
			'bytes'
		], [
      contractAddress,
			getDAOistsData
		])
	}]) ]);
	const { gasUsed: nativeGetGasUsed } = await call('eth_getTransactionReceipt', [ await call('eth_sendTransaction', [{
		to: contractAddress,
		from,
		data: getDAOistsData,
		gas: 6e6,
		gasPrice: 1
	}]) ])
	console.log('getDAOists --')
	console.log('native');
	console.log(Number(nativeGetGasUsed));
	console.log('vm');
	console.log(Number(getGasUsed));
})().catch((err) => console.error(err.stack)); 
