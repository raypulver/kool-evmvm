
'use strict';

const emasm = require('emasm');
const makeConstructor = require('emasm/macros/make-constructor');
const makeJumpTable = require('emasm/macros/jump-table');
const { soliditySha3 } = require('web3-utils');

const ops = require('./ops');

const mapValues = (o, fn) => Object.keys(o).reduce((r, v) => {
	r[v] = fn(o[v], v, o);
	return r;
}, {});

const jumpTable = makeJumpTable('jump-table', Object.assign(Array.from(Array(256).keys()).map(() => 'op:invalid'), mapValues(ops, (v) => 'op:' + v)));

const jumpTableSize = 256*32;

const pushPCLoc = () => [ jumpTableSize ];
const pushPC = () => [ pushPCLoc(), 'mload' ];
/*
const pushCallerLoc = () => [ jumpTableSize + 0x20 ];
const pushCaller = () => [ pushCallerLoc(), 'mload' ];
*/
const pushExtCodeSizeLoc = () => [ jumpTableSize + 0x20 ];
const pushExtCodeSize = () => [ pushExtCodeSizeLoc(), 'mload' ];
const pushExtAddrLoc = () => [ jumpTableSize + 0x40 ];
const pushExtAddr = () => [ pushExtAddrLoc(), 'mload' ];
const pushOffsetLoc = () => [ jumpTableSize + 0x60 ];
const pushOffset = () => [ pushOffsetLoc(), 'mload'];
const makeOffsetMem = (safe) => () => safe ? [
  'dup1',
	pushOffset(),
	'add', // [ offset + loc, loc ]
	'dup1', // [ offset + loc , offset + loc, loc ]
	'dup3', // [ loc, offset + loc, offset + loc, loc ]
	'gt', // [ overflow, offset + loc, loc ]
	'overflow',
	'jumpi',
	'swap1',
	'pop'
] : [ pushOffset(), 'add' ];
const makePushExtCodeLoc = (offset) => () => [ jumpTableSize + offset ];
const pcIncrementAndPush = () => [ pushPC(), '0x1', 'add', 'dup1', pushPCLoc(), 'mstore' ];

const proxyPushCallerLoc = () => [ jumpTableSize + 0x80 ];
const proxyPushAddressLoc = () => [ jumpTableSize + 0xa0 ];
const proxyPushValueLoc = () => [ jumpTableSize + 0xc0 ];
const proxyPushSelfLoc = () => [ jumpTableSize + 0xe0 ];
/*
const proxyPushCallcodeCountLoc = () => [ jumpTableSize + 0x120 ];
const proxyPushDepthLoc = () => [ jumpTableSize + 0x140 ];
*/

const proxyPushExtCodeLoc = () => [ jumpTableSize + 0x100 ];


const opPush = (bytes) => [
	pushPC(),
	'dup1',
	jumpTableSize + 0x81,
	'add',
	'mload',
	(bytes === 32 ? [] : [
		0x100 - bytes*0x8,
  	'shr',
	]),
  'swap1',
	bytes + 0x1,
	'add',
	'dup1',
	pushPCLoc(),
	'mstore'
]; 

const makeInstructionHandlerFactory = (beforeOpHook, replaceFunction, afterOpHook, incrementCounterAndRunInstruction) => (op, code, postInstructions) => [
  'op:' + op,
	[
		beforeOpHook(op) || [], replaceFunction(op) || code || op,
		afterOpHook(op) || [], postInstructions || incrementCounterAndRunInstruction()
	]
]; 

const makeRunNextInstruction = (pushExtCodeLoc) => () => [ // [ pc ]
  pushExtCodeLoc(), 'add', 'mload', '0xf8', 'shr', '0x20', 'mul', 'mload', 'jump'
];

const makeIncrementCounterAndRunInstruction = (pcIncrementAndPush, runNextInstruction) => () => [
  pcIncrementAndPush(),
	runNextInstruction()
];

const computeDelegationProxyAddress = () => [
	'msize', // freeptr
	'msize', // freeptr freeptr
  '0xff00000000000000000000000000000000000000000000000000000000000000', // start freeptr freeptr
	'address', // this start freeptr freeptr
	'0x58', 
	'shl', // this-shifted start freeptr freeptr
	'or', // start freeptr freeptr
	'msize', //freeptr start freeptr freeptr
	'mstore', // freeptr freeptr
	'0x15', 
	'add', // freeptr freeptr-21
	'dup1', // freeptr freeptr freeptr-21
	'0x0', // 0x0 freeptr freeptr freeptr-21
	'swap1', // freeptr 0x0 freeptr freeptr-21
	'mstore', // freeptr freeptr-21
	'0x20',
	'add', // freeptr freeptr-53
	'bytes:delegation-proxy-codehash:size',
	'bytes:delegation-proxy-codehash:ptr',
	'dup3', // freeptr hashptr hashsz freeptr freeptr-53
	'codecopy', // freeptr freeptr-53
	'pop', // freeptr-53
	'0x55', // 0x55 freeptr-53
	'swap1', // freeptr-53 0x55
	'sha3'
];


const makeSecureEvmvm = ({
	initCode = [],
	prelude = [],
	extCodeOffset = 0x80,
	beforeOpHook = () => {},
	replaceFunction = () => {},
	afterOpHook = () => {},
	segments = [],
} = {}) => {
	const pushExtCodeLoc = makePushExtCodeLoc(extCodeOffset);
	const runNextInstruction = makeRunNextInstruction(pushExtCodeLoc);
	const incrementCounterAndRunInstruction = makeIncrementCounterAndRunInstruction(pcIncrementAndPush, runNextInstruction);
	const proxyPushExtCodeLoc = makePushExtCodeLoc(0xe0);
	const proxyRunNextInstruction = makeRunNextInstruction(proxyPushExtCodeLoc);
	const proxyIncrementCounterAndRunInstruction = makeIncrementCounterAndRunInstruction(pcIncrementAndPush, proxyRunNextInstruction);
	const offsetMem = makeOffsetMem(true);
	const proxyHooks = {
		beforeOpHook,
		afterOpHook,
		replaceFunction: (op) => {
			switch (op) {
  			case 'calldataload':
          return [
    				'0xa0',
        		'calldataload',
            '0x20',
            'add',
            'add',
            'calldataload'
  		    ];
  			case 'calldatasize':
  				return [
  					'0xa0',
  					'calldataload',
  					'calldataload'
  				];
  			case 'calldatacopy':
  				return [  
  				  'swap1',
            '0xa0',
            'calldataload',
            '0x20',
            'add',
  				  'add',
  				  'swap1',
            'calldatacopy',
  				];
  			case 'codecopy':
  				return [
            offsetMem(),
            pushExtAddr(),
            'extcodecopy'
  				];
				case 'caller':
          return [
						proxyPushCallerLoc(),
						'mload'
					];
				case 'callvalue':
					return [
						proxyPushValueLoc(),
						'mload'
					];
				case 'address':
					return [
						proxyPushAddressLoc(),
						'mload'
					];
				case 'delegatecall':
          return [
						'msize', // [ freeptr, g, a, in, insize, out, outsize ]
						proxyPushCallerLoc(),
						'mload', // [ caller, freeptr, g, a, in, insize, out, outsize ]
						'dup2',
						'mstore',
						proxyPushAddressLoc(),
						'mload',
						'dup2',
						'mstore',
						proxyPushValueLoc(),
						'mload',
						'dup2',
						'0x40',
						'add',
						'mstore',
						proxyPushSelfLoc(),
						'mload', // [ self, freeptr, g, a, in, insize, out, outsize ]
						'dup1', // [ self, self, freeptr, g, a, in, insize, out, outsize ]
						'dup3', // [ freeptr, self, self, freeptr, g, a, in, insize, out, outsize ]
						'0x60',
						'add',
						'mstore', // [ self, freeptr, g, a, in, insize, out, outsize ]
						'swap3', // [ a, freeptr, g, self, in, insize, out, outsize ]
						'dup2',
						'0x80',
						'add',
						'mstore', // [ freeptr, g, self, in, insize, out, outsize ]
						'0xc0',
						'dup2',
						'0xa0',
						'add', // [ freeptr + 0x80, freeptr, g, self, in, insize, out outsize ]
						'mstore',
            'dup5', // [ insize, freeptr, g, self, in, insize, out, outsize ]
						'dup1', // [ insize, insize, freeptr, g, self, in, insize, out, outsize ]
            'dup3', // [ freeptr, insize, insize, freeptr, g, self, in, insize, out, outsize ]
						'0xc0', 
						'add', // [ freeptr + 0x120, insize, insnize, freeptr, g, self, in, insize, out, outsize ]
						'mstore', // [ insize, freeptr, g, self, in, insize, out, outsize ]
						'iszero',
						'delegatecall-memcpy-return',
						'jumpi',
						'delegatecall-memcpy-return',
						'dup5',
						'dup5',
						offsetMem(),
						'dup3',
						'0xc0',
						'add',
						'0x0',
						'memcpy',
						'jump',
						['delegatecall-memcpy-return', [
							'swap3',
							'pop', // [ g, self, freeptr, insize, out, outsize ]
              'dup4',
							'0xc0',
							'add', // [ insize + 6, g, self, freeptr, insize, out, outsize]
              'swap4',
							'pop', // [ g, self, freeptr, newinsize, out, outsize ]
              'dup5',
							offsetMem(), // [ offset-out, g, self, freeptr, newinsize, out, outsize ]
							'swap5',
              'pop',
              'dup6',
							'dup6',
							'dup6',
							'dup6',
							'dup6',
							'dup6',
							'delegatecall', // [ success, g, self, freeptr, newinsize, out, outsize ]
							'swap6', // [ outsize, g, self, freeptr, newinsize, out, success ]
              'dup5',
							'calldatasize', // [ calldatasize, newinsize, outsize, g, self, freeptr, newinsiez, out, success ]
							'dup6',
							'calldatacopy', // [ outsize, g, self, freeptr, newinsize, out, success ]
							'pop',
							'pop',
							'pop',
							'pop',
							'pop',
							'pop',
							proxyIncrementCounterAndRunInstruction()
						] ]
					];
				case 'callcode':
          return [
						'msize', // [ freeptr, g, a, v, in, insize, out, outsize ]
						proxyPushAddressLoc(),
						'mload', // [ caller, freeptr, g, a, v, in, insize, out, outsize ]
						'dup1',
						'dup3',
						'mstore', // [ caller, freeptr, g, a, v, in, insize, out, outsize ]
						'dup2',
						'0x20',
						'add',
						'mstore', // [ freeptr, g, a, v, in, insize, out, outsize ]
						'dup4',
						'dup2',
						'0x40',
						'add',
						'mstore',
						proxyPushSelfLoc(),
						'mload', // [ self, freeptr, g, a, v, in, insize, out, outsize ]
						'dup1', // [ self, self, freeptr, g, a, v, in, insize, out, outsize ]
						'dup3', // [ freeptr, self, self, freeptr, g, a, v, in, insize, out, outsize ]
						'0x60',
						'add',
						'mstore', // [ self, freeptr, g, a, v, in, insize, out, outsize ]
						'swap3', // [ a, freeptr, g, self, v, in, insize, out, outsize ]
						'dup2',
						'0x80',
						'add',
						'mstore', // [ freeptr, g, self, v, in, insize, out, outsize ]
						'0xc0',
						'dup2',
						'0xa0',
						'add', // [ freeptr + 0x100, 0x120, freeptr, g, self, v, in, insize, out outsize ]
						'mstore',
            'dup6', // [ insize, freeptr, g, self, v, in, insize, out, outsize 
						'dup1',
            'dup2', // [ freeptr, insize, insize, freeptr, g, self, v, in, insize, out, outsize ]
						'0xc0',
						'add',
						'mstore', // [ insize, freeptr, g, self, v, in, insize, out, outsize ]
						'iszero',
						'callcode-memcpy-return',
						'jumpi',
						'callcode-memcpy-return', // [ return, freeptr, g, self, v, in, insize, out, outsize ]
						'dup7', // [ insize, return, freeptr, g, self, v, in,, insize, out, outsize ]
						'dup7', // [ in, insize, return, freeptr, g, self, v, in, insize, out, outsize ]
						offsetMem(),
						// [ offset-in, insize, return, freeptr, g, self, v, in, insize, out, outsize ]
						'dup4',
						'0xe0',
						'add',
						'0x0', 
						'memcpy',
						'jump',
						['callcode-memcpy-return', [
							'swap4', // [ in, g, self, v, freeptr, insize, out, outsize ]
							'pop', // [ g, self, v, freeptr, insize, out, outsize ]
              'dup5',
							'0xe0',
							'add', // [ insize + 0x120, g, self, freeptr, v, insize, out, outsize]
              'swap5',
							'pop', // [ g, self, v, freeptr, newinsize, out, outsize ]
              'dup6',
							offsetMem(), // [ offset+out, g, self, v, freeptr, newinsize, out, outsize ]
							'swap6',
              'pop', // [ g, self, v, freeptr, newinsize, newout, outsize ]
							'swap2',
							'dup1',
							'iszero',
							'perform-callcode',
							'jumpi', // [ v, self, g, freeptr, newinsize, newout, outsize ]
							'msize',
							'msize',
							'bytes:eth-forwarder:size',
							'bytes:eth-forwarder:ptr',
							'msize', // [ new-freeptr, eth-forwarder:ptr, eth-forwarder:size, new-freeptr, new-freeptr, v, self, g, freeptr, newinsize, newout, outsize ]
							'codecopy', // [ new-freeptr, new-freeptr, v, self, g, freeptr, newinsize, newout, outsize ]
              'bytes:eth-forwarder:size',
							'add', // [ insert-address-here, new-freeptr, v, self, g, freeptr, newinsize, newout, outsize ]
              'dup6',
							'0x60',
							'add',
							'mload',
							'swap1',
							'mstore', // [ new-freeptr, v, self, g, freeptr, newinsize, newout, outsize ]
							'bytes:eth-forwarder:size',
							'0x20',
							'add',
							'swap1', // [ eth-forwarder-code, eth-forwarder-size, v, self, g, freeptr, newinsize, newout, outsize ]
              'dup3',
							'create', // [ address, v, self, g, freeptr, newinsize, newout, outsize ]
							'pop' ] ],
  						['perform-callcode', [
                'dup7', // [ outsize, v, self, g, freeptr, newinsize, newout, outsize ]
  							'dup7', // [ newout, outsize, v, self, g, freeptr, newinsize, newout, outsize ]
  							'dup7', // [ newinsize, newout, outsize, v, self, g, freeptr, newinsize, newout, outsize ]
								'dup7', // [ in, insize, out, outsize, v, self, g, freeptr, newinsize, newout, outsize ]
  							'dup6', // [ self, in, insize, out, outsize, v, self, g, in, insize, out, outsize ]
  							'dup8', // [ gas, self, in, insize, out. outsize ]
								'0x0',
								'0x0',
								'log4',
								'0x0',
								'0x0',
								'return',
  							'delegatecall', // [ success, v, self, g, in, insize, out, outsize ]
  							'swap7', // [ outsize, v, self, g, in, insize, out, success ]
                'dup6', // [ insize, outsize, v, self, g, in, insize, out, success ]
	  						'calldatasize', // [ calldatasize, insize, outsize, v, self, g, in, insize, out, success ]
	  						'bytes:eth-forwarder:size',
	  						'add',
	  						'0x20',
	   						'add',
	  						'dup7',
	  						'calldatacopy', // [ outsize,  v, self, g, in, newinsize, out, success ]
	  						'pop',
	  						'pop',
	  						'pop',
	  						'pop',
	  						'pop',
	  						'pop',
	  						'pop',
	  						proxyIncrementCounterAndRunInstruction()
	  					] ]
					];
			}
		}
	};
	const finalSegments = [
		segments,
		[ 'memcpy', [
			'dup1',// i, i, dest, src, size
			'dup4', // src, i, i, dest, src, size
			'add', // word-src, i, dest, src, size
			'mload', // data, i, dest, src, size
			'dup3', // dest, data, i, dest, src, size
			'dup3', // i, dest, data, i, dest, src, size
			'dup1', // i, i, dest, data, i, dest, src, size
			'0x20', // 0x20, i, i, dest, data, i, dest, src, size
			'add', // i + 0x20, i, dest, data, i, dest, src, size
			'dup8', // size, i + 32, i, dest, data, i, dest, src, size
			'lt', // overflow, i, dest, data, i, dest, src, size 
			'partial-memcpy', 
			'jumpi', // i, dest, data, i, dest, src, size
			'add', //  i + dest, data, i, dest, src, size
			'mstore', // i, dest, src, size
			'0x20',
      'add', // new-i, dest, src, size
			'dup4', // size, new-i, dest, src, size
      'dup2', // new-i, size, new-i, dest, src, size
			'lt', // new-i < size, new-i, dest, src, size
			'memcpy',
			'jumpi',
			'finish-memcpy',
			'jump'
		] ],
		[ 'partial-memcpy', [ // i, dest, data, i, dest, src, size
			'add',
			'mload', // mem[dest + i:32], data, i, dest, src, size
			'dup3', // i, mem, data, i, dest, src, size
			'dup7', //  size, i, mem, data, i, dest, src, size
			'sub', // bytes to copy, mem, data, i, dest, src, size
			'dup1',
			'0x20',
			'sub', // bytes to keep, bytes to copy, dest-mem, data, i, dest, src, size
			'0x8',
			'mul', // bits to keep, bytes to copy, dest-mem, data, i, dest, src, size
			'dup1', // bits to keep, bits to keep, bytes to copy, dest-mem, data, i, dest, src, size
			'dup4', // dest-mem, bits to keep, bits to keep, bytes to copy, dest-mem, data, i, dest, src, size
			'swap1',
			'shl', // leftpadded-bytes, bits to keep, bytes to copy, dest-mem, data, i, dest, src, size
			'swap1', // keep-bytes, bytes to copy, dest-mem, data, i, dest, src, size
			'shr',
			'swap4', // data, bytes to copy, dest-mem, keep-bytes, i, dest, src, size
      'swap1',
			'0x8',
			'mul', // bits-to-copy, data, dest-mem, keep-bytes, i, dest, src, size
			'swap1', // data, bits-to-copy, dest-mem, keep-bytes, i, dest, src, size
      'dup2',
			'shr', // copied-bits-left-padded, bits-to-copy, dest-mem, keep-bytes, i, dest, src, size
      'swap1',
			'shl', // new-bytes, dest-mem, keep-bytes, i, dest, src, size
			'swap1',
			'swap2',
			'or', // final-word, dest-mem, i, dest, src, size
      'dup4', // dest, final, dest-mem, i, dest, src, size
			'dup4', // i dest, final, dest-mem, i, dest, src, size
			'add', // i + dest, final, dest-mem, i, dest, src, size
			'mstore', // dest-mem, i, dest, src, size
			'pop'
		] ],
		[ 'finish-memcpy', [
			'pop',
			'pop',
			'pop',
			'pop',
			'jump'
		] ],
		[ 'bytes:eth-forwarder', [ emasm([
			'0x20',
			'dup1',
			'codesize',
			'sub',
			'0x0',
			'codecopy',
			'0x0',
			'mload',
			'selfdestruct'
		]) ] ]
	];
			
	const delegationProxy = emasm(makeEvmvm(Object.assign(proxyHooks, {
		extCodeOffset: 0xe0,
		safeOffset: true,
		segments: finalSegments,
		prelude: [
			'0x0',
			'calldataload',
			proxyPushAddressLoc(),
			'mstore',
			'0x20',
			'calldataload',
			proxyPushCallerLoc(),
			'mstore',
			'0x40',
			'calldataload',
			proxyPushValueLoc(),
			'0x60',
			'calldataload',
			proxyPushSelfLoc(),
			'mstore',
			'0x80',
			'calldataload',
			'dup1',
			'dup1',
			pushExtAddrLoc(),
			'mstore',
      'extcodesize', // [ size, addr ]
			'dup1',
      pushExtCodeSizeLoc(),
			'mstore', // [ size, addr ]
			'0x0', // [ 0x0, size, addr ]
      proxyPushExtCodeLoc(), // [ ptr, 0x0, size, addr ]
			'dup4', // [ addr, ptr, 0x0, size, addr ]
			'extcodecopy',
			'pop',
			proxyPushExtCodeLoc(),
			pushExtCodeSizeLoc(),
			'mload',
			'add',
			'0x1',
			'add',
			pushOffsetLoc(),
			'mstore',
		]
	})));
	return makeEvmvm({
		initCode: [
			'bytes:delegation-proxy:size',
			'bytes:delegation-proxy:ptr',
			'0x0',
			'codecopy',
			'0x0',
			'bytes:delegation-proxy:size',
			'0x0',
			'0x0',
			'create2',
			'bytes:runtime-code:size',
			'bytes:runtime-code:ptr',
			'0x0',
			'codecopy',
			'bytes:runtime-code:size',
			'0x0',
			'return',
			[ 'bytes:delegation-proxy', [ delegationProxy ] ]
		],
		extCodeOffset: 0x80,
		safeOffset: true,
    prelude: [
	  	'calldatasize', pushExtCodeSizeLoc(), 'mstore',
 	  	'calldatasize', '0x0', pushExtCodeLoc(), 'calldatacopy',
      'address', pushExtAddrLoc(), 'mstore', pushExtCodeLoc(), pushExtCodeSizeLoc(), 'mload', 'add', '0x1', 'add', pushOffsetLoc(), 'mstore', prelude
    ],
		segments: [
			...finalSegments,
			[ 'bytes:delegation-proxy-codehash', [
				soliditySha3({
					t: 'bytes',
					v: delegationProxy
				})
			] ]
		],
		beforeOpHook: (proxyHooks.beforeOpHook),
		afterOpHook: (proxyHooks.afterOpHook),
		replaceFunction: (op) => {
  		switch (op) {
				case 'calldataload':
					return [
						'calldatasize',
						'add',
						'calldataload'
					];
				case 'calldatacopy':
					return [
						offsetMem(),
						'swap1',
						'calldatasize',
						'add',
						'swap1',
						'calldatacopy'
					];
				case 'codecopy':
					return [
						offsetMem(),
						'calldatacopy'
					];
				case 'codesize':
					return [
						'calldatasize'
					];
				case 'delegatecall':
          return [
  					'msize', // [ freeptr, g, a, in, insize, out, outsize ]
 						'caller', // [ caller, freeptr, g, a, in, insize, out, outsize ]
						'dup2',
						'mstore',
						'address',
						'dup2',
						'0x20',
						'add',
						'mstore',
						'callvalue',
						'dup2',
						'0x40',
						'add',
						'mstore',
						computeDelegationProxyAddress(),
						'dup1', // [ self, self, freeptr, g, a, in, insize, out, outsize ]
						'dup3', // [ freeptr, self, self, freeptr, g, a, in, insize, out, outsize ]
						'0x60',
						'add',
						'mstore', // [ self, freeptr, g, a, in, insize, out, outsize ]
						'swap3', // [ a, freeptr, g, self, in, insize, out, outsize ]
						'dup2',
						'0x80',
						'add',
						'mstore', // [ freeptr, g, self, in, insize, out, outsize ]
						'0xc0',
						'dup2',
						'0xa0',
						'add', // [ freeptr + 0x100, freeptr, g, self, in, insize, out outsize ]
						'mstore',
            'dup5', // [ insize, freeptr, g, self, in, insize, out, outsize ]
						'dup1', // [ insize, insize, freeptr, g, self, in, insize, out, outsize ]
            'dup3', // [ freeptr, insize, insize, freeptr, g, self, in, insize, out, outsize ]
						'0xc0', // [ 0x120, freeptr, insize, insize, freeptr, g, self, in, insize, out, outsize ]
						'add', // [ freeptr + 0x120, insize, insize, freeptr, g, self, in, insize, out, outsize ]
						'mstore', // [ insize, freeptr, g, self, in, insize, out, outsize ]
						'iszero',
						'delegatecall-memcpy-return',
						'jumpi',
						'delegatecall-memcpy-return', // [ return, freeptr, g, self, in, insize, out, outsize ]
						'dup6', // [ insize, return, freeptr, g, self, in ,insize, out, outsize ]
						'dup6', // [ in, insize, return, freeptr, g, self, in, insize, out, outsize ]
						offsetMem(), // [ newin, insize, return, freeptr, g, self, in, insize, out, outsize ]
						'dup4', // [ freeptr, newin, insize, return, freeptr, g, self, in, insize, out, outsize ]
						'0xe0',
						'add', // freeptr+0x140, newin, insize, return, freeptr, g, self, in, ,insize, out, outsize ]
						'0x0',
						'memcpy', 
						'jump',
						['delegatecall-memcpy-return', [
							'swap3',
							'pop', // [ g, self, freeptr, insize, out, outsize ]
              'dup4',
							'0xe0',
							'add', // [ insize + 0xe0, g, self, freeptr, insize, out, outsize]
              'swap4',
							'pop', // [ g, self, freeptr, newinsize, out, outsize ]
              'dup5',
							offsetMem(), // [ offset+out, g, self, freeptr, newinsize, out, outsize ]
							'swap5',
              'pop',
              'dup6', // [ g, self, freeptr, newinsize, out, outsize ]
							'dup6',
							'dup6',
							'dup6',
							'dup6',
							'dup6',
							'delegatecall', // [ success, g, self, freeptr, newinsize, out, outsize ]
							'swap6', // [ outsize, g, self, freeptr, newinsize, out, success ]
              'dup5',
							'0xe0',
							'add',
							'calldatasize', // [ calldatasize, newinsize, outsize, g, self, freeptr, newinsiez, out, success ]
							'dup6',
							'calldatacopy', // [ outsize, g, self, freeptr, newinsize, out, success ]
							'pop',
							'pop',
							'pop',
							'pop',
							'pop',
							'pop',
							proxyIncrementCounterAndRunInstruction()
						] ]
					];
				case 'callcode':
          return [
						'msize', // [ freeptr, g, a, v, in, insize, out, outsize ]
						'address', // [ address, freeptr, g, a, v, in, insize, out, outsize ]
						'dup2', // [ freeptr, address, freeptr, g, a, v, in, insize, out, outsize ]
						'mstore', // [ freeptr, g, a, v, in, insize, out, outsize ]
						'address', // [ address, freeptr, g, a, v, in, insize, out, outsize ]
						'dup2', // [ freeptr, address, freeptr, g, a, v, in, insize, out, outsize ]
						'0x20',
						'add', // [ freeptr + 0x20, address, freeptr, g, a, v, in, insize, out, outsize ]
						'mstore', // [ freeptr, g, a, v, in, insize, out, outsize ]
						'dup4', // [ v, freeptr, g, a, v, in, insize, out. outsize ]
						'dup2', // [ freeptr, v, freeptr, g, a, v, in, insize, out, outsize ]
						'0x40',
						'add', // [ freeptr + 0x40, v, freeptr, g, a, v, in, insize, out. outsize ]
						'mstore', // [ freeptr, g, a, v, in, insize, out, outsize ]
						computeDelegationProxyAddress(), // [ self, freeptr, g, a, v, in, insize, out, outsize ]
						'dup1', // [ self, self, freeptr, g, a, v, in, insize, out, outsize ]
						'dup3', // [ freeptr, self, self, freeptr, g, a, v, in, insize, out, outsize ]
						'0x60',
						'add', // [ freeptr + 0x60, self, self, freeptr, g, a, v, in, insize, out. outsize ]
						'mstore', // [ self, freeptr, g, a, v, in, insize, out, outsize ]
						'swap3', // [ a, freeptr, g, self, v, in, insize, out, outsize ]
						'dup2', // [ freeptr, a, freeptr, g, self, v, in, insize, out. outsize ]
						'0x80', 
						'add', // [ freeptr + 0x80, a, freeptr, g, self, v, in,. insize, out, outsize ] 
						'mstore', // [ freeptr, g, self, v, in, insize, out, outsize ]
						'0xc0',
						'dup2', // freeptr, 0xc0, freeptr, g, self, v, in, insize, out, outsize ]
						'0xa0',
						'add', // [ freeptr + 0xa0, 0xc0, freeptr, g, self, v, in, insize, out outsize ]
						'mstore', // [ freeptr, g, self, v, in, insize, out, outsize ]
            'dup6', // [ insize, freeptr, g, self, v, in, insize, out, outsize 
						'dup1', // [ insize, insize, freeptr, g, self, v, in, insize, out, outsize ]
            'dup3', // [ freeptr, insize, insize, freeptr, g, self, v, in, insize, out, outsize ]
						'0xc0',
						'add', // [ freeptr + 0xc0, insize, insize, freeptr, g, self, v, in, insize, out, outsize ]
						'mstore', // [ insize, freeptr, g, self, v, in, insize, out, outsize ]
						'iszero',
						'callcode-memcpy-return',
						'jumpi',
						'callcode-memcpy-return', // [ return, freeptr, g, self, v, in, insize, out, outsize ]
						'dup7', // [ insize, return, freeptr, g, self, v, in, insize, out, outsize ]
						'dup7', // [ in, insize, return, freeptr, g, self, v, in, insize, out, outsize ]
						offsetMem(), // [ offset+in, insize, return, freeptr, g, self, v, in, insize, out, outsize ]
						'dup4', // [ freeptr, offset-in, insize, return, freeptr, g, self, v, in, insize, out, outsize ]
						'0xe0',
						'add', // [ freeptr + 0xe0, offset-in, insize, return, freeptr, g, self, v, in, insize, out, outsize ]
//						'dup2',
//						'0x200',
//						'swap1',
//						'log0',
						'0x0', // 0x0, freeptr + 0xe0, offset-in, insize, return
						'memcpy',
						'jump',
						['callcode-memcpy-return', [
							'swap4', // [ in, g, self, v, freeptr, insize, out, outsize ]
							'pop', // [ g, self, v, freeptr, insize, out, outsize ]
              'dup5',
							'0xe0',
							'add', // [ insize + 0x120, g, self, freeptr, v, insize, out, outsize]
              'swap5',
							'pop', // [ g, self, v, freeptr, newinsize, out, outsize ]
              'dup6',
							offsetMem(), // [ offset+out, g, self, v, freeptr, newinsize, out, outsize ]
							'swap6',
              'pop', // [ g, self, v, freeptr, newinsize, newout, outsize ]
							'swap2',
							'dup1',
							'iszero',
							'perform-callcode',
							'jumpi', // [ v, self, g, freeptr, newinsize, newout, outsize ]
							'msize', // [ end-mem, v self, g, freeptr, newinsize, newout, outsize ]
							'msize', // [ end-mem, end-mem, v, self, g, freeptr, newinsize, newout, outsize ]
							'bytes:eth-forwarder:size', // [ eth-forwarder:size, end-mem, end-mem, v, self, g, freeptr, newinsize, newout, outsize ]
							'bytes:eth-forwarder:ptr', // [ eth-forwarder: ptr, end-mem, end-mem, v, self, g, freeptr, newinsize, newout, outsize ]
							'msize', // [ new-freeptr, eth-forwarder:ptr, eth-forwarder:size, new-freeptr, new-freeptr, v, self, g, freeptr, newinsize, newout, outsize ]
							'codecopy', // [ new-freeptr, new-freeptr, v, self, g, freeptr, newinsize, newout, outsize ]
              'bytes:eth-forwarder:size',
							'add', // [ insert-address-here, new-freeptr, v, self, g, freeptr, newinsize, newout, outsize ]
              'dup6', // [ freeptr, insert-address-here, new-freeptr, v, self, g, freeptr, newinsize, newout, outsize ]
							'0x80',
							'add', // [ freeptr + 0x80, insert-address-here, new-freeptr, v, self, g, freeptr, newinsize, newout, outsize ]
							'mload', //  [ a, insert-address-here, new-freeptr, v, self, g, freeptr, newinsize, newout, outsize ]
							'swap1', // [ insert-address-here, a, new-freeptr, v, self, g, freeptr, newinsize, newout, outsize ]
							'mstore', // [ new-freeptr, v, self, g, freeptr, newinsize, newout, outsize ]
							'bytes:eth-forwarder:size', // [ eth-forwarder:size, new-freeptr, v, self, g, freeptr, newinsize, newout, outsize ]
							'0x20',
							'add', // [ eth-forwarder:size + 0x20, new-freeptr, v, self, g, freeptr, newinsize, newout, outsize ]
							'swap1', // [ eth-forwarder-code, eth-forwarder-size + 20, v, self, g, freeptr, newinsize, newout, outsize ]
              'dup3', // [ v, eth-forwarder-code, eth-forwarder-size + 20, v, self, g, freeptr, newinsize, newout, outsize ]
							'create', // [ addr, v, self, g, freeptr, newinsize, newout, outsize ]
							'pop', ] ],
							[ 'perform-callcode', [
                'dup7', // outsize, v, self, g, freeptr, newinsize, newout, outsize ]
  							'dup7', // newout outsize v self g freeptr newinsize newout outsize
  							'dup7', // [ newinsize, newout, outsize, v, self, g, freeptr, newinsize, newout, outsize ]
  							'dup5', // [ self, in, insize, out, outsize, v, self, g, in, insize, out, outsize ]
  							'dup8', // [ gas, self, in, insize, out, outsize, v, self, g, in, insize, out, outsize ]
  							'delegatecall', // [ success, v, self, g, in, insize, out, outsize ]
  							'swap7', // [ outsize, v, self, g, in, insize, out, success ]
                'dup6', // [ insize, outsize, v, self, g, in, insize, out, success ]
	  						'calldatasize', // [ calldatasize, insize, outsize, v, self, g, in, insize, out, success ]
	  						'bytes:eth-forwarder:size',
	  						'add',
	  						'0x20',
	   						'add',
	  						'dup7',
	  						'calldatacopy', // [ outsize,  v, self, g, in, newinsize, out, success ]
	  						'pop',
	  						'pop',
	  						'pop',
	  						'pop',
	  						'pop',
	  						'pop',
	  						'pop',
	  						incrementCounterAndRunInstruction()
	  					] ]
						];
			}
		}
	});
};

const makeStandardEvmvm = ({
	initCode = [],
	prelude = [],
	extCodeOffset = 0x80,
	beforeOpHook = () => {},
	replaceFunction = () => {},
	safeOffset = false,
	afterOpHook = () => {}
} = {}) => {
	const pushExtCodeLoc = makePushExtCodeLoc(extCodeOffset);
	const offsetMem = makeOffsetMem(safeOffset);
	return makeEvmvm({
  	initCode,
		safeOffset,
    prelude: [
	  	'0x0', 'calldataload', 'dup1', 'extcodesize',
 	  	'dup1', pushExtCodeSizeLoc(), 'mstore', '0x0',
      pushExtCodeLoc(), 'dup4', 'extcodecopy', pushExtAddrLoc(),
      'mstore', pushExtCodeLoc(), pushExtCodeSizeLoc(), 'mload',
   	  'add', '0x1', 'add', pushOffsetLoc(), 'mstore', prelude
    ],
	  replaceFunction: (op) => {
  		const override = replaceFunction(op);
  		if (override) return override;
      switch (op) {
  			case 'calldataload':
          return [
    				'0x20',
        		'calldataload',
            '0x20',
            'add',
            'add',
            'calldataload'
  		    ];
  			case 'calldatasize':
  				return [
  					'0x20',
  					'calldataload',
  					'calldataload'
  				];
  			case 'calldatacopy':
  				return [  
						offsetMem(),
  				  'swap1',
            '0x20',
            'calldataload',
            '0x20',
            'add',
  				  'add',
  				  'swap1',
            'calldatacopy',
  				];
  			case 'codecopy':
  				return [
            offsetMem(),
            pushExtAddr(),
            'extcodecopy'
  				];
  		}
  	}
  });
};

const makeEvmvm = ({
  initCode = [],
	prelude = [],
	safeOffset = false,
	extCodeOffset = 0x80,
	beforeOpHook = () => {},
	replaceFunction = () => {},
	afterOpHook = () => {},
	segments = []
} = {}) => {
	const offsetMem = makeOffsetMem(safeOffset);
  const pushExtCodeLoc = makePushExtCodeLoc(extCodeOffset);
	const runNextInstruction = makeRunNextInstruction(pushExtCodeLoc);
	const incrementCounterAndRunInstruction = makeIncrementCounterAndRunInstruction(pcIncrementAndPush, runNextInstruction);
	const makeInstructionHandler = makeInstructionHandlerFactory(beforeOpHook, replaceFunction, afterOpHook, incrementCounterAndRunInstruction);
	return [
  	initCode,
    makeConstructor([
  		[
  			'bytes:jump-table:size', 'bytes:jump-table:ptr', '0x0', 
				'codecopy', prelude, '0x0', runNextInstruction()
			],
  		[ 'execute_jump', [
        '0x1', 'add', 'dup1', pushPCLoc(), 'mstore', runNextInstruction()
  		] ],
      [ 'invalid', 'stop', 'selfdestruct' ].map((label) => makeInstructionHandler(label, [ label ], [])),
      [
  			'add', 'mul', 'sub', 'div', 'sdiv', 'mod', 'smod',
        'addmod', 'mulmod', 'exp', 'signextend', 'lt', 'gt', 'slt',
  			'sgt', 'eq', 'iszero', 'and', 'or', 'xor', 'not',
  			'byte', 'shr', 'shl', 'sar', 'rol', 'ror', 'balance',
        'origin', 'caller', 'callvalue', 'gasprice', 'extcodesize',
        'returndatasize', 'blockhash', 'coinbase',
        'timestamp', 'number', 'difficulty', 'gaslimit', 'pop',
        'sload', 'sstore', 'gas', 'calldataload', 'calldatasize', 'calldatacopy', 'codecopy',
  			...Array.from(Array(16).keys()).map((v) => 'dup' + (v + 1)), 
  			...Array.from(Array(16).keys()).map((v) => 'swap' + (v + 1))
  		].map((label) => makeInstructionHandler(label, [ label ])),
      [
  			'sha3', 'returndatacopy', 'mload', 'mstore', 'mstore8',
  			...Array.from(Array(5).keys()).map((v) => 'log' + v)
  		].map((label) => makeInstructionHandler(label, [ offsetMem(), label ])),
      makeInstructionHandler('address', [ pushExtAddr() ]),
			makeInstructionHandler('codesize', pushExtCodeSize()),
			makeInstructionHandler('extcodecopy', [
				'swap1',
				offsetMem(),
				'swap1',
				'extcodecopy'
			]),
      [ 'op:jump', [
  			'dup1', pushExtCodeLoc(), 'add', 'mload',
        '0xf8', 'shr', '0x5b', 'eq',
        'execute_jump', 'jumpi', '0x0', 'jump'
  		] ],
      [ 'op:jumpi', [
        'swap1',
        'op:jump',
        'jumpi',
        'pop',
  			incrementCounterAndRunInstruction()
  		] ],
  		[ 'op:pc', [
        pushPC(),
  			'dup1',
  			'0x1',
  			'add',
  			'dup1',
  			pushPCLoc(),
  			'mstore',
  			runNextInstruction()
  		] ],
			makeInstructionHandler('msize', [ pushOffset(), 'msize', 'sub' ]),
      [ 'op:jumpdest', incrementCounterAndRunInstruction() ],
      Array.from(Array(32).keys()).map((v) => makeInstructionHandler('push' + (v + 1), [ opPush(v + 1) ], runNextInstruction())),
			makeInstructionHandler('create', [ 'swap1', offsetMem(), 'swap1', 'create' ]),
			makeInstructionHandler('call', [
				'swap3', 
  			offsetMem(),
        'swap3',
        'swap5',
        offsetMem(),
        'swap5',
				'call'
  		]),
  		makeInstructionHandler('callcode', [
        'swap3',
        offsetMem(),
        'swap3',
  			'swap5',
        offsetMem(),
        'swap5',
        'callcode'
  		]),
  		makeInstructionHandler('delegatecall', [
        'swap2',
  			offsetMem(),
        'swap2',
  			'swap4',
        offsetMem(),
        'swap4',
        'delegatecall',
			]),
  		makeInstructionHandler('staticcall', [
        'swap2',
        offsetMem(),
        'swap2',
        'swap4',
        offsetMem(),
  			'swap4',
				'staticcall'
  		]),
			[ 'op:invalid', [ '0x0', 'jump' ]],
  		[ 'return', 'revert' ].map((label) => makeInstructionHandler(label, [
  			offsetMem(), 
  		  label
  		], [])),
  		jumpTable,
			safeOffset ? [ 'overflow', ['0x0', '0x0', 'revert'] ] : [],
			segments
  	])
	];
};
		
module.exports = {
	makeStandardEvmvm,
	makeSecureEvmvm
};
