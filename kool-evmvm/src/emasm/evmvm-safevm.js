
'use strict';

const emasm = require('emasm');
const makeConstructor = require('emasm/macros/make-constructor');
const makeJumpTable = require('emasm/macros/jump-table');

const ops = require('./ops');
const ln = (v) => ((console.log(v)), v);

const mapValues = (o, fn) => Object.keys(o).reduce((r, v) => {
	r[v] = fn(o[v], v, o);
	return r;
}, {});

const jumpTable = makeJumpTable('jump-table', Object.assign(Array.from(Array(256).keys()).map(() => 'op:invalid'), mapValues(ops, (v) => 'op:' + v)));

const jumpTableSize = 256*32;

const pushPCLoc = () => [ jumpTableSize ];
const pushPC = () => [ pushPCLoc(), 'mload' ];
/*
const pushCallerLoc = () => [ jumpTableSize + 0x20 ];
const pushCaller = () => [ pushCallerLoc(), 'mload' ];
*/
const pushExtCodeSizeLoc = () => [ jumpTableSize + 0x20 ];
const pushExtCodeSize = () => [ pushExtCodeSizeLoc(), 'mload' ];
const pushExtAddrLoc = () => [ jumpTableSize + 0x40 ];
const pushExtAddr = () => [ pushExtAddrLoc(), 'mload' ];
const pushOffsetLoc = () => [ jumpTableSize + 0x60 ];
const pushOffset = () => [ pushOffsetLoc(), 'mload'];
const offsetMem = () => [ pushOffset(), 'add' ];
const pushExtCodeLoc = () => [ jumpTableSize + 0x80 ]; 
const pcIncrementAndPush = () => [ pushPC(), '0x1', 'add', 'dup1', pushPCLoc(), 'mstore' ];

const opPush = (bytes) => [
	pushPC(),
	'dup1',
	jumpTableSize + 0x81,
	'add',
	'mload',
	0x100 - bytes*0x8,
	'shr',
	'swap1',
	bytes + 0x1,
	'add',
	'dup1',
	pushPCLoc(),
	'mstore'
]; 

const makeInstructionHandlerFactory = (beforeOpHook, replaceFunction, afterOpHook) => (op, code, postInstructions) => [
  'op:' + op,
	[
		beforeOpHook(op) || [], replaceFunction(op) || code || op,
		afterOpHook(op) || [], postInstructions || incrementCounterAndRunInstruction()
	]
]; 

const runNextInstruction = () => [ // [ pc ]
  pushExtCodeLoc(), 'add', 'mload', '0xf8', 'shr', '0x20', 'mul', 'mload', 'jump'
];

const incrementCounterAndRunInstruction = () => [
  pcIncrementAndPush(),
	runNextInstruction()
];

const makeDelegateProxy = ({
  initCode = [],
	prelude = [],
	beforeOpHook = () => {},
	replaceFunction = () => {},
	untrustedExecution = true,
	afterOpHook = () => {},
} = {}) => {
	const makeInstructionHandler = makeInstructionHandlerFactory(beforeOpHook, afterOpHook);
	return [
  	initCode,
  	makeConstructor([
  		[
  			'bytes:jump-table:size', 'bytes:jump-table:ptr', '0x0', 'codecopy',
        '0x0', 'calldataload', pushCallerLoc(), 'mstore',
				'0x20', 'calldataload', 'dup1', 'extcodesize', 'dup1',
				pushExtCodeSizeLoc(), 'mstore', '0x0', pushExtCodeLoc(),
				'dup4', 'extcodecopy', pushExtAddrLoc(), 'mstore',
				pushExtCodeLoc(), pushExtCodeSizeLoc(), 'mload', 'add',
				'0x1', 'add', pushOffsetLoc(), 'mstore', '0x0',
  			runNextInstruction()
  	  ],
  		[ 'execute_jump', [
        '0x1', 'add', 'dup1', pushPCLoc(), 'mstore', runNextInstruction()
  		] ],
      [ 'invalid', 'stop', 'selfdestruct' ].map((label) => makeInstructionHandler(label, [ label ], [])),
      [
  			'add', 'mul', 'sub', 'div', 'sdiv', 'mod', 'smod',
        'addmod', 'mulmod', 'exp', 'signextend', 'lt', 'gt', 'slt',
  			'sgt', 'eq', 'iszero', 'and', 'or', 'xor', 'not',
  			'byte', 'shr', 'shl', 'sar', 'rol', 'ror', 'balance',
        'origin', 'caller', 'callvalue', 'gasprice', 'extcodesize',
        'extcodecopy', 'returndatasize', 'blockhash', 'coinbase',
        'timestamp', 'number', 'difficulty', 'gaslimit', 'pop',
        'sload', 'sstore', 'gas',
  			...Array.from(Array(16).keys()).map((v) => 'dup' + (v + 1)), 
  			...Array.from(Array(16).keys()).map((v) => 'swap' + (v + 1))
  		].map((label) => makeInstructionHandler(label, [ label ])),
      [
  			'sha3', 'returndatacopy', 'mload', 'mstore', 'mstore8',
  			...Array.from(Array(5).keys()).map((v) => 'log' + v)
  		].map((label) => makeInstructionHandler(label, [ offsetMem(), label ])),
      makeInstructionHandler('address', [ pushExtAddr() ]),
			makeInstructionHandler('calldataload', [
        '0x40',
  			'calldataload',
        '0x20',
        'add',
        'add',
        'calldataload'
  		]),
  		makeInstructionHandler('calldatasize', [ '0x40', 'calldataload', 'calldataload' ]),
      makeInstructionHandler('calldatacopy', [
        offsetMem(),
				'swap1',
        '0x40',
        'calldataload',
        '0x20',
        'add',
				'add',
				'swap1',
        'calldatacopy',
  		]),
			makeInstructionHandler('codesize', pushExtCodeSize()),
			makeInstructionHandler('codecopy', [
        offsetMem(),
        pushExtAddr(),
        'extcodecopy'
			]),
      [ 'op:jump', [
  			'dup1', pushExtCodeLoc(), 'add', 'mload',
        '0xf8', 'shr', '0x5b', 'eq',
        'execute_jump', 'jumpi', '0x0', 'jump'
  		] ],
      [ 'op:jumpi', [
        'swap1',
        'op:jump',
        'jumpi',
        'pop',
  			incrementCounterAndRunInstruction()
  		] ],
  		[ 'op:pc', [
        pushPC(),
  			'dup1',
  			'0x1',
  			'add',
  			'dup1',
  			pushPCLoc(),
  			'mstore',
  			runNextInstruction()
  		] ],
			makeInstructionHandler('msize', [ pushOffset(), 'msize', 'sub' ]),
      [ 'op:jumpdest', []],
      Array.from(Array(32).keys()).map((v) => makeInstructionHandler('push' + (v + 1), [ opPush(v + 1) ], runNextInstruction())),
			makeInstructionHandler('create', [ 'swap1', offsetMem(), 'swap1', 'create' ]),
			makeInstructionHandler('call', [
				'swap3', 
  			offsetMem(),
        'swap3',
        'swap5',
        offsetMem(),
        'swap5',
				'call'
  		]),
  		makeInstructionHandler('callcode', [
        'swap3',
        offsetMem(),
        'swap3',
  			'swap5',
        offsetMem(),
        'swap5',
        'callcode'
  		]),
  		makeInstructionHandler('delegatecall', [
        'swap2',
  			offsetMem(),
        'swap2',
  			'swap4',
        offsetMem(),
        'swap4',
        'delegatecall',
			]),
  		makeInstructionHandler('staticcall', [
        'swap2',
        offsetMem(),
        'swap2',
        'swap4',
        offsetMem(),
  			'swap4',
				'staticcall'
  		]),
			[ 'op:invalid', [ '0x0', 'jump' ]],
  		[ 'return', 'revert' ].map((label) => makeInstructionHandler(label, [
  			offsetMem(), 
  		  label
  		], [])),
  		jumpTable
  	])
	];
};

const makeEvmvm = ({
  initCode = [],
	prelude = [],
	beforeOpHook = () => {},
	replaceFunction = () => {},
	untrustedExecution = true,
	afterOpHook = () => {},
} = {}) => {
	const makeInstructionHandler = makeInstructionHandlerFactory(beforeOpHook, replaceFunction, afterOpHook);
	return [
  	initCode,
  	makeConstructor([
  		[
  			'bytes:jump-table:size', 'bytes:jump-table:ptr', '0x0', 'codecopy',
        '0x0', 'calldataload', 'dup1', 'extcodesize',
  			'dup1', pushExtCodeSizeLoc(), 'mstore', '0x0',
        pushExtCodeLoc(), 'dup4', 'extcodecopy', pushExtAddrLoc(),
        'mstore', pushExtCodeLoc(), pushExtCodeSizeLoc(), 'mload',
  			'add', '0x1', 'add', pushOffsetLoc(), 'mstore', '0x0',
  			runNextInstruction()
  	  ],
  		[ 'execute_jump', [
        '0x1', 'add', 'dup1', pushPCLoc(), 'mstore', runNextInstruction()
  		] ],
      [ 'invalid', 'stop', 'selfdestruct' ].map((label) => makeInstructionHandler(label, [ label ], [])),
      [
  			'add', 'mul', 'sub', 'div', 'sdiv', 'mod', 'smod',
        'addmod', 'mulmod', 'exp', 'signextend', 'lt', 'gt', 'slt',
  			'sgt', 'eq', 'iszero', 'and', 'or', 'xor', 'not',
  			'byte', 'shr', 'shl', 'sar', 'rol', 'ror', 'balance',
        'origin', 'caller', 'callvalue', 'gasprice', 'extcodesize',
        'extcodecopy', 'returndatasize', 'blockhash', 'coinbase',
        'timestamp', 'number', 'difficulty', 'gaslimit', 'pop',
        'sload', 'sstore', 'gas',
  			...Array.from(Array(16).keys()).map((v) => 'dup' + (v + 1)), 
  			...Array.from(Array(16).keys()).map((v) => 'swap' + (v + 1))
  		].map((label) => makeInstructionHandler(label, [ label ])),
      [
  			'sha3', 'returndatacopy', 'mload', 'mstore', 'mstore8',
  			...Array.from(Array(5).keys()).map((v) => 'log' + v)
  		].map((label) => makeInstructionHandler(label, [ offsetMem(), label ])),
      makeInstructionHandler('address', [ pushExtAddr() ]),
			makeInstructionHandler('calldataload', [
        '0x20',
  			'calldataload',
        '0x20',
        'add',
        'add',
        'calldataload'
  		]),
  		makeInstructionHandler('calldatasize', [ '0x20', 'calldataload', 'calldataload' ]),
      makeInstructionHandler('calldatacopy', [
        offsetMem(),
				'swap1',
        '0x20',
        'calldataload',
        '0x20',
        'add',
				'add',
				'swap1',
        'calldatacopy',
  		]),
			makeInstructionHandler('codesize', pushExtCodeSize()),
			makeInstructionHandler('codecopy', [
        offsetMem(),
        pushExtAddr(),
        'extcodecopy'
			]),
      [ 'op:jump', [
  			'dup1', pushExtCodeLoc(), 'add', 'mload',
        '0xf8', 'shr', '0x5b', 'eq',
        'execute_jump', 'jumpi', '0x0', 'jump'
  		] ],
      [ 'op:jumpi', [
        'swap1',
        'op:jump',
        'jumpi',
        'pop',
  			incrementCounterAndRunInstruction()
  		] ],
  		[ 'op:pc', [
        pushPC(),
  			'dup1',
  			'0x1',
  			'add',
  			'dup1',
  			pushPCLoc(),
  			'mstore',
  			runNextInstruction()
  		] ],
			makeInstructionHandler('msize', [ pushOffset(), 'msize', 'sub' ]),
      [ 'op:jumpdest', []],
      Array.from(Array(32).keys()).map((v) => makeInstructionHandler('push' + (v + 1), [ opPush(v + 1) ], runNextInstruction())),
			makeInstructionHandler('create', [ 'swap1', offsetMem(), 'swap1', 'create' ]),
			makeInstructionHandler('call', [
				'swap3', 
  			offsetMem(),
        'swap3',
        'swap5',
        offsetMem(),
        'swap5',
				'call'
  		]),
  		makeInstructionHandler('callcode', [
        'swap3',
        offsetMem(),
        'swap3',
  			'swap5',
        offsetMem(),
        'swap5',
        'callcode'
  		]),
  		makeInstructionHandler('delegatecall', [
        'swap2',
  			offsetMem(),
        'swap2',
  			'swap4',
        offsetMem(),
        'swap4',
        'delegatecall',
			]),
  		makeInstructionHandler('staticcall', [
        'swap2',
        offsetMem(),
        'swap2',
        'swap4',
        offsetMem(),
  			'swap4',
				'staticcall'
  		]),
			[ 'op:invalid', [ '0x0', 'jump' ]],
  		[ 'return', 'revert' ].map((label) => makeInstructionHandler(label, [
  			offsetMem(), 
  		  label
  		], [])),
  		jumpTable
  	])
	];
};
		
module.exports = makeEvmvm;
