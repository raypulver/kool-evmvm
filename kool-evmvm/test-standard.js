'use strict';

const rpcCall = require('kool-makerpccall');
const emasm = require('emasm');
const abi = require('web3-eth-abi');
const makeConstructor = require('emasm/macros/make-constructor');
const easySolc = require('./easy-solc');
const fs = require('fs');
const path = require('path');
const compile = require('../src/compile-huff');
const { expect } = require('chai');

const { encodeFunctionCall: encodeFunctionCallDestructure, encodeParameters: encodeParametersDestructure } = abi;

const encodeFunctionCall = encodeFunctionCallDestructure.bind(abi);
const encodeParameters = encodeParametersDestructure.bind(abi);

const call = (method, params = []) => rpcCall('http://localhost:8545', method, params);

describe('evmvm', () => {
	it('should run opcodes', async () => {
    const [ from ] = await call('eth_accounts');
    const { bytecode } = easySolc.compile('Test', fs.readFileSync(path.join(__dirname, './Test.sol'), 'utf8'));
		const { contractAddress } = await call('eth_getTransactionReceipt', [ await call('eth_sendTransaction', [{
			gasPrice: 1,
			gas: 6e6,
			from,
			data: bytecode
		}]) ]);
    const evmvmBytecode = compile();
		const { contractAddress: evmvmContractAddress } = await call('eth_getTransactionReceipt', [ await call('eth_sendTransaction', [{
			gasPrice: 1,
			gas: 6e6,
			from,
			data: evmvmBytecode
		}]) ]);
		const ln = (v) => ((console.log(v)), v);
		await call('eth_sendTransaction', [{
			gasPrice: 1,
			gas: 6e6,
			to: evmvmContractAddress,
			from,
			data: (encodeParameters(['address', 'bytes'], [ contractAddress, (encodeFunctionCall({
				name: 'setB',
				inputs: [{
					name: 'b',
					type: 'uint256'
				}]
			}, [ 10 ])) ]))
		}]);
		expect(Number(await call('eth_call', [{
			to: evmvmContractAddress,
			gas: 6e6,
			gasPrice: 1,
			from,
      data: encodeParameters(['address', 'bytes'], [ contractAddress, encodeFunctionCall({
				name: 'b',
				inputs: []
			}, []) ])
		}]))).to.eql(11);
		expect(Number(await call('eth_call', [{
			to: evmvmContractAddress,
			gas: 6e6,
			gasPrice: 1,
			from,
			data: encodeParameters(['address', 'bytes'], [ contractAddress, encodeFunctionCall({
				name: 'getBPlusFive',
				inputs: []
			}, []) ])
		}]))).to.eql(16);
		/*
		const { contractAddress: sample } = await call('eth_getTransactionReceipt', [ await call('eth_sendTransaction', [{
			from,
			gas: 6e6,
			gasPrice: 1,
			data: emasm(makeConstructor([
				'0x0',
				'calldataload',
				'0xff',
				'eq',
				'woop',
				'jumpi',
				'0x1',
				'0x2',
				'add',
				'0x0',
				'mstore',
				'0x20',
				'0x0',
				'return',
				['woop', [
					'0x40',
					'0x0',
					'return'
				]]
			]))
		}]) ]);
		*/
		/*
		console.log(await call('eth_call', [{
			to: sample,
			data: '0x' + Array(63).join('0') + 'ff'
		}]));
		*/
	});
});
