pragma solidity ^0.5.0;
contract Test {
  uint256 public b;
  event Log();
  function setB(uint256 _b) public returns (bool) {
    b = _b + 1;
    return true;
  }
  function getBPlusFive() public returns (uint256) {
    return b + 5;
  }
  function () external {
    emit Log();
  }
}
