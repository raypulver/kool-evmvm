'use strict';

const emasm = require('emasm');
const makeConstructor = require('emasm/macros/make-constructor');

const { makeSecureEvmvm } = require('../src/emasm/evmvm');
const rpcCall = require('kool-makerpccall');

const call = (method, params = []) => rpcCall('http://localhost:8545', method, params);
const { expect } = require('chai');

describe('secure evmvm', () => {
	it('should work', async () => {
	  const [ from ] = await call('eth_accounts');	
    const makeContract = async (data) => (await call('eth_getTransactionReceipt', [ await call('eth_sendTransaction', [{
	    from,
			data,
			gas: 100e6,
			gasPrice: 1
		}]) ])).contractAddress;
		const vmAddress = await makeContract(emasm(makeSecureEvmvm()));
	  const result = await call('eth_call', [{
			to: vmAddress,
			data: emasm([
				'0x0',
				'calldataload',
				'0x10',
				'mul',
				'pc',
				'0x0',
				'mstore',
				'0x20',
				'0x0',
				'return'
			])
		}]);
		const callcodeTarget = await makeContract(emasm(makeConstructor([
			'0x0',
			'calldataload',
			'balance',
			'0x0',
			'mstore',
			'address',
			'0x20',
			'mstore',
			'caller',
			'0x40',
			'mstore',
			'0x60',
			'0x0',
			'return'
		])));
		const callcodeClient = await call('eth_sendTransaction', [{
			to: vmAddress,
			from,
			value: '0x1000',
			gas: 6e6,
			gasPrice: 1,
			data: emasm([
  			'0x0',
  			'0x0',
  			'0x20',
  			'0x0',
  			'0x1000',
				callcodeTarget,
				'dup1',
				'0x0',
				'mstore',
				'gas',
				'callcode',
        'returndatasize',
				'dup1',
				'0x0',
				'0x0',
				'returndatacopy',
				'0x0',
				'log0'
			])
		}]);
		console.log((await call('eth_getTransactionReceipt', [ callcodeClient ])).logs);
	})
});
